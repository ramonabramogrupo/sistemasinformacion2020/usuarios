<?php

use yii\db\Schema;
use yii\db\Migration;

class m210403_210020_usuarios extends Migration
{

    public function init()
    {
        $this->db = 'db';
        parent::init();
    }

    public function safeUp()
    {
        $tableOptions = 'ENGINE=InnoDB';

        $this->createTable(
            '{{%usuarios}}',
            [
                'id'=> $this->primaryKey(11),
                'nombre'=> $this->string(255)->null()->defaultValue(null),
                'apellidos'=> $this->string(255)->null()->defaultValue(null),
                'email'=> $this->string(50)->notNull(),
                'username'=> $this->string(55)->notNull(),
                'authKey'=> $this->string(255)->notNull(),
                'activo'=> $this->integer(1)->null()->defaultValue(0),
                'password'=> $this->string(255)->null()->defaultValue(null),
                'accessToken'=> $this->string(255),
                'role' =>$this->integer(1)->notNull()->defaultValue(1),
            ],$tableOptions
        );
        $this->createIndex('UK_usuarios_email','{{%usuarios}}',['email'],true);
        $this->createIndex('UK_usuarios_username','{{%usuarios}}',['username'],true);

    }

    public function safeDown()
    {
        $this->dropIndex('UK_usuarios_email', '{{%usuarios}}');
        $this->dropIndex('UK_usuarios_username', '{{%usuarios}}');
        $this->dropTable('{{%usuarios}}');
    }
}
